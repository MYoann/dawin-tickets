import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-persons',
  templateUrl: './persons.component.html',
  styleUrls: ['./persons.component.scss']
})
export class PersonsComponent {

  constructor(private router: Router) { }

  goToTicketsModule() {
    this.router.navigate(['/tickets']);
  }
}

import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/index';
import { Person } from './person';

@Injectable({
  providedIn: 'root'
})
export class PersonsService {
  public persons$: BehaviorSubject<Person[]>;

  constructor() {
    this.persons$ = new BehaviorSubject([
      { name: 'Philippe Lacombe', age: '24' } as Person,
      { name: 'Olivier Dubois', age: '47' } as Person,
      { name: 'Marc Dupont', age: '20' } as Person,
      { name: 'Pierre Richard', age: '29' } as Person,
      { name: 'Emmanuel Poiteau', age: '18' } as Person,
      { name: 'Jacques Bonamy', age: '78' } as Person
    ]);
  }
}

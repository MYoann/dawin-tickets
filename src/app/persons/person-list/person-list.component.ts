import { Component, OnDestroy, OnInit } from '@angular/core';

import { Person } from '../person';
import { PersonsService } from '../persons.service';
import { Subscription } from 'rxjs/index';

@Component({
  selector: 'app-person-list',
  templateUrl: './person-list.component.html',
  styleUrls: ['./person-list.component.scss']
})
export class PersonListComponent implements OnInit, OnDestroy {
  public persons: Person[];
  private subscription: Subscription;

  constructor(private personsService: PersonsService) {
  }

  ngOnInit() {
    this.subscription = this.personsService.persons$.subscribe((persons) => {
      this.persons = persons;
    });
  }

  ngOnDestroy() {
    // Unsubscribe is needed to avoid memory leak
    this.subscription.unsubscribe();
  }
}

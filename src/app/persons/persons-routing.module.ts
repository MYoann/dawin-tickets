import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PersonsComponent } from './persons.component';
import { PersonListComponent } from './person-list/person-list.component';

const routes: Routes = [{
  path: '',
  component: PersonsComponent,
  children: [
    { path: '', redirectTo: 'list' },
    { path: 'list', component: PersonListComponent },
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PersonsRoutingModule { }

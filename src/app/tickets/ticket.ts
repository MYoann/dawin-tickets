import { TICKET_PRIORITY } from './ticket-priority';

export interface Ticket {
  id: string;
  message: string;
  priority: TICKET_PRIORITY;
  estimation: number;
}

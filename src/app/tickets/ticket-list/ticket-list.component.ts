import { BehaviorSubject, combineLatest, Observable, Subscription } from 'rxjs';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { map } from 'rxjs/internal/operators';
import { TicketsService } from '../tickets.service';
import { Ticket } from '../ticket';
import { TICKET_PRIORITY } from '../ticket-priority';

@Component({
  selector: 'app-ticket-list',
  templateUrl: './ticket-list.component.html',
  styleUrls: ['./ticket-list.component.scss']
})
export class TicketListComponent implements OnInit, OnDestroy {
  public tickets: Ticket[];
  public showLowPriorityTickets = true;
  public showLowPriorityTickets$: BehaviorSubject<boolean>;
  private subscription: Subscription;

  constructor(private ticketService: TicketsService) {
  }

  ngOnInit() {
    this.showLowPriorityTickets$ = new BehaviorSubject(this.showLowPriorityTickets);

    this.subscription = combineLatest(
      this.showLowPriorityTickets$,
      this.ticketService.getTickets()
    ).pipe(
      map(([showLowPriorityTickets, tickets]) =>
        showLowPriorityTickets ? tickets : tickets.filter((ticket) => ticket.priority !== TICKET_PRIORITY.LOW)
      )
    ).subscribe((tickets) => {
      this.tickets = tickets;
    });
  }

  ngOnDestroy() {
    // Don't forget to unsubscribe or you'll get memory leaks
    this.subscription.unsubscribe();
  }

  public onChangeShowLowPriorityTickets(showLowPriorityTickets) {
    this.showLowPriorityTickets$.next(showLowPriorityTickets);
  }

  public generateNewTicket() {
    this.ticketService.addTicket(this.ticketService.generateNewTicket());
  }
}

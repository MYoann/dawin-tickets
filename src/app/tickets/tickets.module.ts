import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TicketComponent } from './ticket/ticket.component';
import { TicketListComponent } from './ticket-list/ticket-list.component';
import { TicketsComponent } from './tickets.component';
import { TicketsRoutingModule } from './tickets-routing.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [TicketComponent, TicketListComponent, TicketsComponent],
  imports: [
    CommonModule,
    TicketsRoutingModule,
    FormsModule
  ]
})
export class TicketsModule {
}

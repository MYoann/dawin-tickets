import { Injectable } from '@angular/core';
import { BehaviorSubject, interval } from 'rxjs/index';
import { Ticket } from './ticket';
import { TICKET_PRIORITY } from './ticket-priority';

@Injectable({
  providedIn: 'root'
})
export class TicketsService {
  public tickets$: BehaviorSubject<Ticket[]>;

  constructor() {
    const tickets: Ticket[] = [];

    for (let i = 0; i < 12; i++) {
      tickets.push(this.generateNewTicket());
    }

    this.tickets$ = new BehaviorSubject(tickets);
  }

  public getTickets(): BehaviorSubject<Ticket[]> {
    return this.tickets$;
  }

  public generateNewTicket(): Ticket {
    return {
      id: Date.now().toString(),
      message: 'Message n°' + Date.now(),
      estimation: (Math.floor(Math.random() * 6) + 1) * 4,
      priority: this.getRandomTicketPriority()
    } as Ticket;
  }

  private getRandomTicketPriority(): TICKET_PRIORITY {
    return TICKET_PRIORITY[Object.keys(TICKET_PRIORITY)[Math.floor(Math.random() * Object.keys(TICKET_PRIORITY).length)]];
  }

  public addTicket(ticket: Ticket) {
    // Copy array value to don't push the same array reference as new value of the behaviorSubject
    // splice(0) will just copy the array value, similar to [...this.tickets$.getValue()]
    const tickets = this.tickets$.getValue().splice(0);
    tickets.push(ticket);
    this.tickets$.next(tickets);
  }
}

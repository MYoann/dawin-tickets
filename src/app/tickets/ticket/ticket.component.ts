import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Ticket } from '../ticket';
import { TICKET_PRIORITY } from '../ticket-priority';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.scss']
})
export class TicketComponent {
  @Input() ticket: Ticket;
  @Output() close = new EventEmitter();

  TICKET_PRIORITY = TICKET_PRIORITY;
}
